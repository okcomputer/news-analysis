#!/usr/bin/env python

import unittest
import os
import sys
import re
import json
import datetime
sys.path.insert(0, os.path.dirname(__file__))
import fx_articles

class TestData(unittest.TestCase):
    ''' '''
    def setUp(self):
        '''
        '''
        self.data_source = fx_articles.FxData("https://rss.dailyfx.com/feeds/all")
        self.find_url = re.compile("(https|http)")
        self.single_url = "https://www.dailyfx.com/forex/education/trading_tips/daily_trading_lesson/2018/08/03/differences-between-dow-nasdaq-and-sp-500.html"

    def test_article_urls(self):
        '''
        ensure we return a url (needs work)
        '''
        data = self.data_source.article_urls()
        self.assertTrue(self.find_url.match(data[0]))

    def test_article_download(self):
        '''
        ensure we return json object
        '''
        article_dowload = self.data_source.article_download(self.single_url)
        self.assertTrue(json.loads(json.dumps(article_dowload)))


    def test_article_content(self):
        '''
        ensure you are able to json serilization
        this will need to change
        '''
        article_text = self.data_source.article_content()
        self.assertTrue(json.loads(json.dumps(article_text)))

    def test_article_date(self):
        '''
        test if the string received can be converted to datetime object
        '''
        today = datetime.date.today()
        today_string  = self.data_source.article_date(today)
        date_object = datetime.datetime.strptime(today_string, '%d %B %Y')
        self.assertIsInstance(date_object, datetime.date)


if __name__ == "__main__":
    unittest.main()
