#!/usr/bin/env python

import newspaper
import feedparser
from collections import defaultdict
import sys
import json


class FxData(object):
    ''' '''
    def __init__(self, url):
        ''' '''
        self.article_url = url
        self.urls = []
        self.data = defaultdict(dict)
        self.date_format = '%d %B %Y'

    def article_urls(self):
        '''
        '''
        data = feedparser.parse(self.article_url)
        for articles in data.entries:
            self.urls.append(articles.link)
        return self.urls

    def article_download(self, article_url):
        '''
        '''
        article_obj = newspaper.Article(article_url)
        article_obj.download()
        article_obj.parse()
        clean_data = self.sanitize_content(article_obj)
        return clean_data

    def article_content(self):
        '''
        '''
        urls = self.article_urls()
        for url in urls:
            article = self.article_download(url)
            # here is where we would post to the flask api
            break
        return article


    def sanitize_content(self, article_text):
        '''
        '''
        article_text.nlp()
        self.data['content'] = article_text.text.replace('\n', '.')
        self.data['keywords'] = article_text.keywords
        self.data['author'] = article_text.authors
        self.data['_time'] = self.article_date(article_text.publish_date)
        return self.data

    def article_date(self, date_object):
        '''
        '''
        return date_object.strftime(self.date_format)


if __name__ == "__main__":
    data = FxData("https://rss.dailyfx.com/feeds/all")
    data = data.article_content()
